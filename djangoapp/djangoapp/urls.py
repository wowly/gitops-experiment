from django.contrib import admin
from django.urls import path

from .views import HelloWordView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', HelloWordView.as_view())
]
